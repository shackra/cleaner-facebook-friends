package main

import (
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
)

func main() {
	m := martini.Classic()
	m.Use(martini.Static("bootstrap", martini.StaticOptions{
		Prefix: "static/",
	}))
	m.Use(render.Renderer(render.Options{
		Extensions: []string{".gohtml", ".gtl"},
		Layout:     "layout",
	}))

	m.Get("/", func(r render.Render) {
		r.HTML(200, "index", nil)
	})

	m.Run()
}
